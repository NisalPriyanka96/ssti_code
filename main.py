from flask import Flask, request, render_template_string

app = Flask(__name__)

@app.route('/tmp-injection')
def hello_ssti():
    person = {'name':"flag", 'secret':"SGF5WW91R290TXlTZWNyZXQ="}
    if request.args.get('name'):
        person['name'] = request.args.get('name')
        template = '''<h2>Hello %s!</h2>''' % person['name']
        return render_template_string(template, person=person)
    
    template = '<h2> Hello World! </h2>'
    return render_template_string(template)

@app.route('/tmp-injection-safe')
def hello_ssti_safe():
    person = {'name':"flag", 'secret':"SGF5WW91R290TXlTZWNyZXQ="}
    if request.args.get('name'):
        person['name'] = request.args.get('name')
        template = """ <h1> Hello {{person}}! </h1> """
        return render_template_string(template, person=person['name'])
    
    template = '<h2> Hello World! </h2>'
    return render_template_string(template)


####
# Private function if the user has local files.
###
def get_user_file(f_name):
    with open(f_name) as f:
        return f.readlines()

app.jinja_env.globals['get_user_file'] = get_user_file # Allows for use in Jinja2 templates

if __name__ == "__main__":
    app.run(debug=True)



